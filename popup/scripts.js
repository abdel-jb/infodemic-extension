chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    if (!tabs[0].url.includes('https://twitter.com')) {
        let container = document.querySelector('body .container');
        container.style.display = "none";
        let p = document.createElement("p");
        p.innerText = "This extension is not available for current page. Please go to https://twitter.com/";
        p.style = "font-size: 15px; font-weight: bold; color: darkred; padding:20px";
        document.body.appendChild(p);
    }
});

window.onload = () => {
    const name_state = 'infodemic-state';
    const name_translate = 'infodemic-translate';
    const svg = document.querySelector('.btn-pwf-svg');
    const btn_translate =  document.querySelector('.cont-buttons_switch input');
    let state;
    chrome.storage.sync.get([name_state, name_translate], result => {
        if (result[name_state]) {
            state = result[name_state];
        }else{
            chrome.storage.sync.set({[name_state]: 'on'})
            state = 'on';
        }
        state === 'on' ? svg.style.fill = '#0097e6' : svg.style.fill = '#e84118';

        result[name_translate] ? btn_translate.checked = true : btn_translate.checked = false;
    });

    svg.addEventListener('click', () => {
        chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
            if (state === 'off') {
                chrome.storage.sync.set({[name_state]: 'on'});
                state = 'on';
                svg.style.fill = '#0097e6';
            } else if (state === 'on') {
                chrome.storage.sync.set({[name_state]: 'off'});
                state = 'off';
                svg.style.fill = '#e84118';
            }
            console.log(state)
            chrome.tabs.sendMessage(tabs[0].id, { state });
        });
    });

    btn_translate.addEventListener('click', ({ target : { checked} }) =>{
        chrome.tabs.query({active: true, currentWindow: true}, (tabs)  => {
            if (checked) {
                chrome.storage.sync.set({[name_translate]: true});
                checked = true;
            }else{
                chrome.storage.sync.set({[name_translate]: false});
                checked = false;
            }
            chrome.tabs.sendMessage(tabs[0].id, { checked });
        });
    });
}